import imagePreload from '../src/utils/imagePreload';

describe('imagePreload', () => {

  const imageUrl = 'test.jpg';

  test('should return the image url after preloading', () => {
    imagePreload(imageUrl).then(image => {
      expect(image).toBe(imageUrl)
    })
  })

})