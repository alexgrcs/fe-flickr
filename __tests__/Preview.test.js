import React from 'react';
import { shallow } from 'enzyme';
import Preview from '../src/components/Preview';
import PreviewImage from '../src/components/PreviewImage';
import { itemMock } from '../__mocks__';

describe('<Preview />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<Preview item={itemMock} />).dive();
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  test('should render PreviewImage component without throwing an error', () => {
    expect(wrapper.find(PreviewImage).exists()).toBe(true)
  })

  test('should render a title without throwing an error', () => {
    expect(wrapper.find('Title').exists()).toBe(true)
  })
})