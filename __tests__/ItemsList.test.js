import React from 'react';
import { shallow } from 'enzyme';
import ItemsList from '../src/components/ItemsList';
import Item from '../src/components/Item';
import { itemsListMock } from '../__mocks__';

describe('<ItemsList />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<ItemsList items={itemsListMock} />).dive();
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('ul').exists()).toBe(true)
  })

  test('should render Item components', () => {
    expect(wrapper.find(Item).exists()).toBe(true)
  })
})