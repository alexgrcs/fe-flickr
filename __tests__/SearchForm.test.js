import React from 'react';
import { shallow } from 'enzyme';
import SearchForm from '../src/components/SearchForm';

describe('<SearchForm />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<SearchForm onSubmit={() => 'test'} />);
  })

  test('should render a form without throwing an error', () => {
    expect(wrapper.name()).toEqual('form')
  })

  test('should render an input without throwing an error', () => {
    expect(wrapper.find('Input').exists()).toBe(true)
  })

  test('should execute the submit prop', () => {
    let e = {};
    e.preventDefault = f => f;
    let test = wrapper.instance().handleOnsubmit(e);
    expect(test).toEqual(false)
  })
})