import React from 'react';
import { shallow } from 'enzyme';
import Thumbnail from '../src/components/Thumbnail';

const imageUrl = 'test.jpg';

describe('<Thumbnail />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<Thumbnail imageUrl={imageUrl} />).dive();
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  test('should render the thumbnail image', () => {
    expect(wrapper.find('.thumb').exists()).toBe(true)
  })
})