import { getPhotos, parseError } from '../src/api';

describe('api', () => {

  const options = {
    method: 'flickr.photos.search',
    tag_mode: 'all',
    per_page: 30,
    page: 1,
    format: 'json',
    media: 'photos',
    sort: 'interestingness-desc',
    nojsoncallback: 1,
    text: 'cat',
    page: 1
  }

  test('should return pictures data from Flickr', () => {
    return getPhotos(options).then(data => {
      expect(data).toHaveProperty('data');
    })
  })

  test('should return an error message', () => {
    const message = 'Error message';
    const error = parseError(message);
    
    expect(error).toHaveProperty('message', message)
  });

})