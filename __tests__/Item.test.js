import React from 'react';
import { shallow } from 'enzyme';
import Item from '../src/components/Item';
import Thumbnail from '../src/components/Thumbnail';
import { itemMock } from '../__mocks__';

describe('<Item />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<Item item={itemMock} />).dive();
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  test('should render Thumbnail component', () => {
    expect(wrapper.find(Thumbnail).exists()).toBe(true)
  })
})