Flickr App
===========

## Configuración
Instalar dependencias
```sh
$ npm install
```

## Development
Para ejecutar el webpack-dev-server local y autocompilar en [http://localhost:8080/](http://localhost:8080/)
```sh
$ npm start
```

## Unit Test
Para ejecutar los unit test
```sh
$ npm run test
```

## Deployment
Para hacer un build de la aplicación
```sh
$ npm run build
```
