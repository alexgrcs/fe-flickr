import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { getPhotos, getPhotoData } from '../api';
import ItemsList from '../components/ItemsList';
import SearchForm from '../components/SearchForm';
import Preview from '../components/Preview';

class App extends React.Component {
  state = {
    text: '',
    page: 1,
    modal: false,
    items: null,
    preview: null,
    loading: false
  }
  
  componentDidMount() {
    this.setPreview();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.text !== this.state.text) this.loadImages();

    if (prevState.preview !== this.state.preview) this.setBodyOverflow(this.state.preview);
    
    if (this.props.match.params === prevProps.match.params) return false;

    this.setPreview();
  }

  setPreview = () => {
    const { match } = this.props;

    if (!match || !match.params || !match.params.picture) {
      this.setState({ preview: null });
      return false
    };

    this.setImage(match.params.picture);    
  }

  setBodyOverflow = (overflow = false) => {
    document.body.style.overflow = overflow ? 'hidden' : 'auto';
  }

  setImage = picture => {
    const { items } = this.state;
    const currentItem = items && items.data && items.data.find(item => item.id === picture);

    if (currentItem) this.setState({ preview: currentItem });
    else getPhotoData(picture).then(preview => this.setState({ preview }));
  } 
  
  loadImages = () => {
    this.setState({ page: 1, loading: true }, () => {
      const { text, page } = this.state;
      const options = { text, page };
    
      this.setState({ items: null });

      getPhotos(options).then(items => this.setState({ items, loading: false }));
    });
  }

  loadImagesPage = () => {
    this.setState({ page: this.state.page + 1, loading: true }, () => {
      const { text, page, items } = this.state;
      const options = { text, page };

      getPhotos(options).then(({ data }) => {
        const oldData = items.data;
        const newItemsData = [...oldData, ...data];
        const newItems = { data: newItemsData };

        this.setState({ items: newItems, loading: false });
      });
    });
  }

  handleOnSubmit = text => this.setState({ text, page: 1 });

  render() {
    const { items, text, preview, loading } = this.state;

    return (
      <MainWrap>
        <SearchForm onSubmit={this.handleOnSubmit} />
        {items && <RenderContainer {...items} addPreview={this.addPreview} />}
        {loading && <Loading text={text} />}
        {items && <LoadMoreItems onClick={this.loadImagesPage} />}
        {preview && <Preview item={preview} />}
      </MainWrap>
    );
  }
}

const LoadMoreItems = ({ onClick }) => (
  <LoadMoreWrap>
    <button onClick={onClick}>Load More Images</button>
  </LoadMoreWrap>
)

const RenderContainer = ({ data, message, addPreview }) =>
  data ?
    <ItemsList items={data} addPreview={addPreview} /> :
    <ErrorMessage message={message} />;

const Loading = ({ text }) => text && <LoadingWrap>Loading images for "{text}"...</LoadingWrap>;

const ErrorMessage = ({ message }) => <LoadingWrap>{message}</LoadingWrap>;

RenderContainer.propTypes = {
  data: PropTypes.array,
  message: PropTypes.string,
  openPreview: PropTypes.func
}

ErrorMessage.propTypes = {
  message: PropTypes.string
}

const MainWrap = styled.div`
  display: block;
  max-width: 920px;
  min-height: 400px;
  padding: 0px 40px;
  margin: 100px auto;
`;

const LoadMoreWrap = styled.div`
  text-align: center;
  margin: 40px auto;
  button {
    font-size: 20px;
    color: inherit;
    padding: 10px;
    cursor: pointer;
    border: 0;
    background: #fff;
    box-shadow: 0px 4px 7px rgba(0,0,0,.16);
    -webkit-appearance: none;
  }
`;

const LoadingWrap = styled.div`
  text-align: center;
  margin: 30px auto;
  color: #b3b2b0;
`;

export default App;