/**
 * Error message object
 * @param {String} error 
 */
const getErrorMessage = error => (
  { message: error || 'Sorry, an error has occurred. :(' }
)

export default getErrorMessage;