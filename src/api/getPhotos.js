/**
 * Flickr API
 * Get Flickr photos from text
 */
import qs from 'qs';
import axios from 'axios';
import { getPhotoData, parseError } from './index';
import { FLICKR_URL, FLICKR_KEY } from '../constants';

/**
 * Set Flickr photos search
 * @param {Object} options 
 */
const getPhotos = options => {
  const defaultQuery = {
    method: 'flickr.photos.search',
    api_key: FLICKR_KEY,
    tag_mode: 'all',
    per_page: 50,
    page: 1,
    format: 'json',
    media: 'photos',
    sort: 'interestingness-desc',
    nojsoncallback: 1,
  };

  const query = { ...defaultQuery, ...options };

  return getPhotosRequest(query);
}

/**
 * Flickr photos GET request
 * @param {Object} query 
 */
export async function getPhotosRequest(query) {
  let response = axios.get(`${FLICKR_URL}${qs.stringify(query)}`);
  let data = await response;

  if (!data.data.photos || !data.data.photos.photo) return parseError();

  let itemsData = await data.data.photos.photo.map(item => getPhotoData(item.id));

  let items = await Promise.all(itemsData);

  // Remove items with no image
  let validItems = items.filter(item => !!item);

  return { data: validItems };
}

export default getPhotos;