/**
 * Flickr API
 * Get and parse Flickr photo data from id
 */

import { getPhotoInfo, getPhotoSizes } from './index';

async function getPhotoData(id) {
  let info = await getPhotoInfo(id);
  let sizes = await getPhotoSizes(id);

  const { imageurl, thumburl } = sizes;

  // Return null if image is not longer available
  if (!imageurl || ! thumburl) return null;

  return { ...info, ...sizes };
}

export default getPhotoData;