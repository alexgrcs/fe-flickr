/**
 * Flickr API
 * Get Flickr photos with different sizes from id
 */

import qs from 'qs';
import axios from 'axios';
import { parseError } from './index';
import { FLICKR_URL, FLICKR_KEY } from '../constants';

/**
 * Set Flickr photo sizes query
 * @param {Object} options 
 */
const getPhotoSizes = photo_id => {
  const query = {
    method: 'flickr.photos.getSizes',
    api_key: FLICKR_KEY,
    format: 'json',
    nojsoncallback: 1,
    photo_id
  }

  return getPhotoSizesRequest(query);
}

/**
 * Flickr photo sizes GET request
 * @param {Object} query 
 */
const getPhotoSizesRequest = query =>
  axios.get(`${FLICKR_URL}${qs.stringify(query)}`)
    .then(res => {
      if (!res.data || !res.data.sizes || !res.data.sizes.size) return [];
      
      return parseSizes(res.data.sizes.size);
    })
    .catch(error => parseError(error));

/**
 * Parse photo sizes
 * @param {Object} data 
 */
const parseSizes = photoSizes => {
  
  const thumb = photoSizes.find(item => {
    const { label } = item;

    return (label === 'Small 320') || (label === 'Medium') || (label === 'Small');
  });
  
  const image = photoSizes.find(item => {
    const { label } = item;

    return (label === 'Large') || (label === 'Large 1600') || (label === 'Large 2048');
  });
  
  const selectedSizes = {
    thumburl: thumb.source,
    imageurl: image.source
  }

  return selectedSizes;
}

export default getPhotoSizes;