/**
 * Flickr API
 * Get Flickr photo info from id
 */

import qs from 'qs';
import axios from 'axios';
import { parseError } from './index';
import { FLICKR_URL, FLICKR_KEY, FLICKR_SECRET } from '../constants';

/**
 * Set Flickr photo info query
 * @param {Object} options 
 */
const getPhotoInfo = id => {
  const query = {
    method: 'flickr.photos.getInfo',
    api_key: FLICKR_KEY,
    format: 'json',
    nojsoncallback: 1,
    photo_id: id,
    secret: FLICKR_SECRET
  }

  return getPhotoInfoRequest(query);
}

/**
 * Flickr photo info GET request
 * @param {Object} query 
 */
const getPhotoInfoRequest = query =>
  axios.get(`${FLICKR_URL}${qs.stringify(query)}`)
    .then(res => {
      if (!res.data || !res.data.photo) return null;

      return parseInfo(res.data.photo);
    })
    .catch(error => parseError(error));

/**
 * Parse photo info
 * @param {Object} data 
 */
const parseInfo = ({ id, title, owner, description, dates, secret, tags, urls, views }) => ({
  id,
  title: title['_content'],
  owner,
  ownerid: owner.nsid,
  description: description['_content'],
  posturl: urls.url[0]['_content'],
  dates,
  secret,
  tags,
  views
})

export default getPhotoInfo;