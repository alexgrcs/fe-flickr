import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class SearchForm extends React.Component {

  handleOnsubmit = e => {
    e.preventDefault();
    
    // Check if some value exists in input
    if (!this.input || !this.input.value) {
      return false;
    }

    this.props.onSubmit(this.input.value);
    this.input.value = '';
  }

  render() {
    return (
      <form onSubmit={this.handleOnsubmit}>
        <Label>Search Photos</Label>
        <InputWrap>
          <Input innerRef={input => this.input = input} />
          <SubmitButton type="submit">
              <SearchIcon  viewBox="0 0 32 32">
                <path d="M31.008 27.231l-7.58-6.447c-0.784-0.705-1.622-1.029-2.299-0.998 1.789-2.096 2.87-4.815 2.87-7.787 0-6.627-5.373-12-12-12s-12 5.373-12 12 5.373 12 12 12c2.972 0 5.691-1.081 7.787-2.87-0.031 0.677 0.293 1.515 0.998 2.299l6.447 7.58c1.104 1.226 2.907 1.33 4.007 0.23s0.997-2.903-0.23-4.007zM12 20c-4.418 0-8-3.582-8-8s3.582-8 8-8 8 3.582 8 8-3.582 8-8 8z"></path>
              </SearchIcon>
          </SubmitButton>
        </InputWrap>
      </form>
    )
  }
}

SearchForm.proptypes = {
  onSubmit: PropTypes.func.isRequired
}

const Label = styled.div`
  font-size: 10px;
  margin-bottom: 1.4em;
  text-transform: uppercase;
`;

const InputWrap = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  margin-bottom: 80px;
  box-shadow: 0px 4px 7px rgba(0,0,0,.16);
`;

const Input = styled.input`
  display: block;
  width: calc(100% - 64px);
  font-size: 18px;
  line-height: 2.4em;
  padding: 0 1em;
  border: 0;
  color: #a2a19f;
`;

Input.displayName = 'Input'

const SubmitButton = styled.button`
  height: 40px;
  width: 64px;
  background: #cccccc;
  border: 0;
  margin-right: 4px;
  cursor: pointer;
`;

const SearchIcon = styled.svg`
  display: inline-block;
  width: 2em;
  height: 2em;
  stroke-width: 0;
  stroke: #ffffff;
  fill: #ffffff;
`;

export default SearchForm;