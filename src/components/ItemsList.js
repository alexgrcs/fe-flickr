import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Item from './Item';
import media from '../utils/media';

const ItemsList = ({ items }) => (
  <ItemsWrap>
    {items.map((item, index) => (
      <Item key={`item-${index}`} item={item} />
    ))}
  </ItemsWrap>
)

ItemsList.propTypes = {
  items: PropTypes.array
}

const ItemsWrap = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: left;
  margin: -5px;
  padding: 0;
  ${ media.tablet`
    justify-content: center;
  `}
`;

export default ItemsList;