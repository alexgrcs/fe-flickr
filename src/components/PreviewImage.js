import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import imagePreload from '../utils/imagePreload';

class PreviewImage extends React.Component {
  state = { imageUrl: null }

  componentDidMount() {
    this.props.imageUrl &&
    imagePreload(this.props.imageUrl).then(imageUrl => this.setState({ imageUrl }));
  }

  render() {
    return <Image className="image" bg={this.state.imageUrl} />;
  }
}

PreviewImage.propTypes = {
  image: PropTypes.string
}

const Image = styled.div.attrs({
  style: props => ({
    backgroundImage: props.bg && `url(${props.bg})`,
  })
})`
  display: block;
  position: absolute;
  top: 0;
  left: 20px;
  width: calc(100% - 40px);
  height: 100%;
  background-size: contain;
  background-position: 50%;
  background-repeat: no-repeat;
`;

export default PreviewImage;