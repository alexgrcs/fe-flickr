import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Thumbnail from './Thumbnail';
import media from '../utils/media';

const Item = ({ item }) => {
  return (
    <ItemWrap>
      <Title>{item.title}</Title>
      <Author>by <a href={item.posturl} target="_blank">{item.owner.username}</a></Author>
      <Link to={`/${item.id}`}>
        <Thumbnail imageUrl={item.imageurl} />
      </Link>
    </ItemWrap>
  );
}

Item.propTypes = {
  item: PropTypes.object,
  openPreview: PropTypes.func
}

const ItemWrap = styled.div`
  flex: 1 0 25%;
  max-width: 304px;
  height: 248px;
  margin: 10px;
  background-color: #FFFFFF;
  text-align: right;
  overflow: hidden;
  ${ media.tablet`
    flex: 1 0 33.333%;
  `}
  ${ media.mobile`
    flex: 1 0 50%;
  `}
`;

const Title = styled.h2`
  min-height: 19px;
  font-size: 15px;
  font-weight: 400;
  margin: 0;
  color: #85a3a1;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Author = styled.span`
  display: block;
  font-size: 10px;
  line-height: 2;
  color: #b3b2b0;
  a {
    color: #959394;
    text-decoration: none;
  }
`;

export default Item;