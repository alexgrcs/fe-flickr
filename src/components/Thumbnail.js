import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import imagePreload from '../utils/imagePreload';

class Thumbnail extends React.Component {
  state = { imageUrl: null }

  componentDidMount() {
    this.props.imageUrl &&
    imagePreload(this.props.imageUrl).then(imageUrl => this.setState({ imageUrl }));
  }

  render() {
    return (
      <ThumbWrap onClick={this.props.onClick}>
        <ThumbImage className="thumb" bg={this.state.imageUrl} />
      </ThumbWrap>
    )
  }
}

Thumbnail.propTypes = {
  image: PropTypes.string
}

const ThumbWrap = styled.div`
  display: block;
  padding: 6px;
  box-shadow: 0px 4px 7px rgba(0,0,0,.16);
  cursor: pointer;
`;

const ThumbImage = styled.div.attrs({
  style: props => ({
    backgroundImage: props.bg && `url(${props.bg})`,
  })
})`
  display: block;
  width: 100%;
  padding-bottom: 66%;
  background-position: 50%;
  background-size: cover;
  background-repeat: no-repeat;
  background-color: rgba(0,0,0,.16);
`;

export default Thumbnail;