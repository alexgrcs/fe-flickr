import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import moment from 'moment';
import PreviewImage from './PreviewImage';
import media from '../utils/media';

const defaultColor = '#a2a19f';
const primaryColor = '#85a3a1';
const softColor = '#b3b2b0';

const formatDate = value => moment.unix(value).format("DD/MM/YYYY");

const Preview = ({ item }) => {
  const { owner, dates, tags, views, posturl, id } = item;

  return (
    <PreviewLayer>
      <Link to={'/'}>
        <CloseButton viewBox="0 0 32 32">
          <path d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
        </CloseButton>
      </Link>
      <PreviewWrap>
        <Media>
          <PreviewImage imageUrl={item.imageurl} />
        </Media>
        <Content>
          <Title>{item.title}</Title>
          {item.description && <p dangerouslySetInnerHTML={{__html: item.description}} />}
          <Data>
            {owner.realname && <li><span>Name: </span>{owner.realname}</li>}
            {owner.username && <li><span>Username: </span>{owner.username}</li>}
            {owner.location && <li><span>Location: </span>{owner.location}</li>}
          </Data>
          <Data>
            {dates.posted && <li><span>Posted: </span>{formatDate(dates.posted)}</li>}
            {dates.lastupdate && <li><span>Last update: </span>{formatDate(dates.lastupdate)}</li>}
          </Data>
          {tags && tags.tag && tags.tag.length > 0 &&
            <Tags>
              {tags.tag.map(tagItem =>
                <li key={tagItem.id}>#{tagItem._content}</li>)
              }
            </Tags>
          }
          <Data>
            {views && <li><span>Views: </span>{views}</li>}
          </Data>
          <Data>
            {posturl &&
              <li>
                <span>Original post: </span>
                <a href={posturl} target="_blank">
                  {id}
                </a>
              </li>
            }
          </Data>
        </Content>
      </PreviewWrap>
    </PreviewLayer>
  )
}

Preview.propTypes = {
  item: PropTypes.object,
  removePreview: PropTypes.func
}

const PreviewLayer = styled.div`
  display: flex;
  position: fixed;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  z-index: 2;
  width: calc(100% - 80px);
  height: calc(100% - 80px);
  padding: 40px;
  overflow-x: hidden;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  background-color: rgba(255, 255, 255, 0.95);
`;

const PreviewWrap = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  height: 100%;
  ${ media.tablet`
    flex-direction: column;
  `}
`;

const CloseButton = styled.svg`
  display: block;
  position: absolute;
  width: 30px;
  height: 30px;
  top: 30px;
  right: 30px;
  z-index: 3;
  cursor: pointer;
  stroke-width: 0;
  fill: ${softColor};
`;

const Media = styled.div`
  position: relative;
  width: 70%;
  padding: 20px 10px 20px 20px;
  ${ media.tablet`
    width: 100%;
    min-height: 100%;
  `}
`;

const Content = styled.div`
  width: 30%;
  padding: 20px 20px 20px 10px;
  a {
    color: ${defaultColor};
  }
  ${ media.tablet`
    width: 100%;
  `}
`;

const Title = styled.h2`
  font-weight: normal;
  color: ${primaryColor};
`;

Title.displayName = 'Title';

const Data = styled.ul`
  padding: 0;
  list-style: none;
  span {
    font-weight: 200;
    color: ${softColor};
  }
`

const Tags = styled.ul`
  padding: 0;
  list-style: none;
  li {
    display: inline-block;
    margin-right: 1em;
    color: ${primaryColor};
  }
`;

export default Preview;