/**
 * Style utils
 */
import { css } from 'styled-components';

const media = {
  tablet: (...args) => css`
    @media (max-width: 1024px) {
      ${ css(...args)}
    }
  `,
  mobile: (...args) => css`
    @media (max-width: 768px) {
      ${ css(...args)}
    }
  `
}

export default media;