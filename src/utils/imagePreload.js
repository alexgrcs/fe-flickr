/**
 * Image preload helper
 */

const imagePreload = url =>
  new Promise((resolve, reject) => {
    const img = new Image();
    img.src = url;
    
    img.onload = () => resolve(url);
    img.onerror = e => reject(url);
  });

export default imagePreload;