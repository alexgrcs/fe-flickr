const itemMock = {
  "id": "16044466914",
  "title": "Vestrahorn in Iceland",
  "owner": {
    "nsid": "10465727@N00",
    "username": "Nick L",
    "realname": "Nick Leonard",
    "location": "Poole, UK",
    "iconserver": "851",
    "iconfarm": 1,
    "path_alias": "nickleo"
  },
  "ownerid": "10465727@N00",
  "description": "Iceland October 2014 at sunset\n\nI shot 90 minutes of pictures from just before sunset till about an hour after the Sun went down and I am still working on them. This one is my favourite so far, a beautiful place that is worth a visit if you are in the area.\nShot from Stokksnes in South East Iceland",
  "posturl": "https://www.flickr.com/photos/nickleo/16044466914/",
  "dates": {
    "posted": "1523707083",
    "taken": "2014-10-20 18:03:45",
    "takengranularity": "0",
    "takenunknown": "0",
    "lastupdate": "1531752177"
  },
  "secret": "7a548467cf",
  "tags": {
    "tag": [
      {
        "id": "3140556-16044466914-2269386",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Vestrahorn",
        "_content": "vestrahorn",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-9375197",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Vesturhorn",
        "_content": "vesturhorn",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-59042928",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Kambhorn",
        "_content": "kambhorn",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-18536148",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Brunnhorn",
        "_content": "brunnhorn",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-6448488",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Stokksnes",
        "_content": "stokksnes",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-799853",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Hofn",
        "_content": "hofn",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-399",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Mountain",
        "_content": "mountain",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-499",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Mountains",
        "_content": "mountains",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-2045",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Black Sand",
        "_content": "blacksand",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-2371",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Iceland",
        "_content": "iceland",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-9239076",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "South East Iceland",
        "_content": "southeasticeland",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-228",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Sea",
        "_content": "sea",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-85817337",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "EOS 5D Mk III",
        "_content": "eos5dmkiii",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-39312892",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "EOS 5D Mark III",
        "_content": "eos5dmarkiii",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-1966",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "EOS",
        "_content": "eos",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-83600122",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "EOS 5D Mark 3",
        "_content": "eos5dmark3",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-71597802",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Canon 5D Mark 3",
        "_content": "canon5dmark3",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-1382",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Canon",
        "_content": "canon",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-23146679",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Canon 16-35L II",
        "_content": "canon1635lii",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-1345720",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "16-35L",
        "_content": "1635l",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-292610",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "16-35",
        "_content": "1635",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-244634415",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "LaTierraUnParaiso",
        "_content": "latierraunparaiso",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-65056037",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Canon 5D3",
        "_content": "canon5d3",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-39633643",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "5D3",
        "_content": "5d3",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-5485518",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "Southern Iceland",
        "_content": "southerniceland",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-950813",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "South Iceland",
        "_content": "southiceland",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-142581895",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "October 2014",
        "_content": "october2014",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-197391525",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "stokkness",
        "_content": "stokkness",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-16654110",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "stokknes",
        "_content": "stokknes",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-26165",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "horn",
        "_content": "horn",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-19959907",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "stockness",
        "_content": "stockness",
        "machine_tag": 0
      },
      {
        "id": "3140556-16044466914-3184",
        "author": "10465727@N00",
        "authorname": "Nick L",
        "raw": "H3",
        "_content": "h3",
        "machine_tag": 0
      }
    ]
  },
  "views": "54072",
  "thumburl": "https://farm9.staticflickr.com/8649/16044466914_7a548467cf_m.jpg",
  "imageurl": "https://farm9.staticflickr.com/8649/16044466914_7a548467cf_b.jpg"
};

export default itemMock;