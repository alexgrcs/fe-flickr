const itemsListMock = [
  {
    farm: 9,
    id: "8420618551",
    isfamily: 0,
    isfriend: 0,
    ispublic: 1,
    owner: "39070943@N04",
    secret: "edfc4ee81c",
    server: "8083",
    title: "hello.....anyone there.....?"
  },
  {
    farm: 9,
    id: "8420618551",
    isfamily: 0,
    isfriend: 0,
    ispublic: 1,
    owner: "39070943@N04",
    secret: "edfc4ee81c",
    server: "8083",
    title: "hello.....anyone there.....?"
  }
]

export default itemsListMock;